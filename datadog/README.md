# minikube install
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v1.3.1/minikube-darwin-amd64 \
  && chmod +x minikube && cp minikube /usr/local/bin/minikube

# install datadog monitoring and update daemon set agent datadog apm and logs
helm install --name datadog-monitoring \
-f value.yaml \
--set datadog.apiKey=$token_datadog \
--set datadog.appKey=$appkey_datadog \
--set clusterAgent.enabled=true \
--set clusterAgent.metricsProvider.enabled=true \
stable/datadog

# setting helm chart datadog for trace APM
https://app.datadoghq.com/apm/docs?architecture=container-based&collection=From%20Helm%20Chart&environment=kubernetes&language=node